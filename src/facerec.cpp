#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
//#include <opencv2/opencv.hpp>
//
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>


#include <facerec/import.h>
#include <facerec/libfacerec.h>

//using namespace cv;
namespace py = pybind11;

void load_file(const std::string path, std::vector<uint8_t> *data);

pbio::RawSample::Ptr capture_single_face(const std::string image_file, const pbio::Capturer::Ptr capturer);

void load_file(const std::string path, std::vector<uint8_t> *data)
{
    if (!data)
    {
        throw std::runtime_error("load_file: got NULL data pointer");
    }

    data->clear();

    std::ifstream file(path.c_str(), std::ios_base::binary);

    if (!file.is_open())
    {
        throw std::runtime_error("load_file: file '" + path + "' not opened");
    }

    file.seekg(0, file.end);
    const size_t len = (size_t)file.tellg();

    file.seekg(0, file.beg);

    data->resize(len);

    file.read((char*)data->data(), len);

    if (!file.good())
    {
        throw std::runtime_error("load_file: error reading file '" + path + "'");
    }
}

pbio::RawSample::Ptr capture_single_face(const std::string image_file, const pbio::Capturer::Ptr capturer)
{
    // load image
    std::vector<uint8_t> image_data;
    load_file(image_file, &image_data);
    // capture face sample
    const std::vector<pbio::RawSample::Ptr> samples = capturer->capture(image_data.data(), image_data.size());
    if (samples.size() != 1)
    {
        throw std::runtime_error("faces count not 1");
    }
    return samples[0];
}

//import py3divilib
//dll_path = "facerec.dll"
//conf_dir_path = "../../../face_sdk/conf/facerec"
//capturerName = "common_capturer4.xml"
//t = py3divilib.detect("faces.png", dll_path, conf_dir_path, capturerName)
//print(t)

//recognizerMethod = "method7v3_recognizer.xml"
//templateFile = "templates.bin"
//imageFile = "01100.jpg"
//t = py3divilib.recognize(imageFile, dll_path, conf_dir_path, capturerName, recognizerMethod, templateFile)
//print(t)

PYBIND11_MODULE(py3divilib, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring

    /*m.def("detect", &detect, R"pbdoc(
        3divi detect

        TBD:
    )pbdoc", py::arg("imageFile"), py::arg("dll_path"), py::arg("conf_dir_path"), py::arg("capturerName"));*/

    /*m.def("recognize", &recognize, R"pbdoc(
        3divi recognize

        TBD:
    )pbdoc", py::arg("imageFile"), py::arg("dll_path"), py::arg("conf_dir_path"), py::arg("capturerName"), 
        py::arg("recognizerMethod"), py::arg("templateFile"));*/

    m.def("detect", [](const std::string& imageFile, const std::string& dll_path,
        const std::string& conf_dir_path, const std::string& capturerName) ->  std::vector<std::map<std::string, int>> {
        /*return imageFile + " detect";*/
        std::vector<std::map<std::string, int>> res;
        try
        {
            std::vector<std::string> image_files;
            image_files.push_back(imageFile);

            // create facerec service (initialize facerec)
            const pbio::FacerecService::Ptr service = pbio::FacerecService::createService(dll_path, conf_dir_path);

            // create capturer
            const pbio::Capturer::Ptr capturer = service->createCapturer(capturerName);

            // create quality estimator
            const pbio::QualityEstimator::Ptr quality_estimator = service->createQualityEstimator("quality_estimator.xml");

            // create age and gender estimator
            const pbio::AgeGenderEstimator::Ptr age_gender_estimator = service->createAgeGenderEstimator("age_gender_estimator.xml");

            // create emotions estimator
            const pbio::EmotionsEstimator::Ptr emotions_estimator = service->createEmotionsEstimator("emotions_estimator.xml");

            for (size_t file_i = 0; file_i < image_files.size(); ++file_i)
            {
                const std::string filepath = image_files[file_i];

                //std::cout << "\n\n\nProcessing file '" << filepath << "' ..." << std::endl;

                // load image file
                std::vector<uint8_t> file_data;
                load_file(filepath, &file_data);

                // capture face samples
                std::vector<pbio::RawSample::Ptr> samples = capturer->capture(file_data.data(), file_data.size());

                std::cout << samples.size() << " face(s) captured" << std::endl;

                for (size_t i = 0; i < samples.size(); ++i)
                {
                    // get sample info
                    const pbio::RawSample::Rectangle rect = samples[i]->getRectangle();
                    std::map<std::string, int> t;
                    t.insert(std::pair<std::string, int>("x", rect.x));
                    t.insert(std::pair<std::string, int>("y", rect.y));
                    t.insert(std::pair<std::string, int>("width", rect.width));
                    t.insert(std::pair<std::string, int>("height", rect.height));
                    res.push_back(t);
                }
            }
        }
        catch (const pbio::Error &e)
        {
            std::cerr << "facerec exception catched: '" << e.what() << "' code: " << std::hex << e.code() << std::endl;
        }
        catch (const std::exception &e)
        {
            std::cerr << "exception catched: '" << e.what() << "'" << std::endl;
        }
        return res;
    });

    m.def("recognize", [](const std::string& imageFile, const std::string& dll_path, const std::string& conf_dir_path,
        const std::string& capturerName, const std::string& recognizerMethod, const std::string& templateFile) -> std::vector<std::map<std::string, std::string>> {
        std::vector<std::map<std::string, std::string>> res;
        const pbio::FacerecService::Ptr service = pbio::FacerecService::createService(dll_path, conf_dir_path);
        const pbio::Capturer::Ptr capturer = service->createCapturer(capturerName);
        const pbio::Recognizer::Ptr recognizer = service->createRecognizer(recognizerMethod);

        const pbio::RawSample::Ptr the_sample = capture_single_face(imageFile, capturer);
        std::cout << "image captured" << std::endl;
        // processing sample
        const pbio::Template::Ptr the_template = recognizer->processing(*the_sample);
        std::cout << "image processed" << std::endl;
        // load templates
        std::ifstream templates_file(templateFile.c_str(), std::ios_base::binary);
        if (!templates_file.is_open())
        {
            throw std::runtime_error("'" + templateFile + "' file not opened");
        }
        int count = 0;
        templates_file.read((char*)&count, sizeof(count));
        std::vector<std::string> names;
        std::vector<pbio::Template::Ptr> templates;
        for (int i = 0; i < count; ++i)
        {
            // load name
            std::string name;
            for (;; )
            {
                char s;
                templates_file.read(&s, sizeof(char));
                if (!s)
                    break;
                name.push_back(s);
            }
            // load template
            const pbio::Template::Ptr face_template = recognizer->loadTemplate(templates_file);
            // -
            names.push_back(name);
            templates.push_back(face_template);
        }
        templates_file.close();
        // identification
        // match the_template with all others
        // vector<pair<distance, name>>
        std::vector<std::tuple<double, std::string, double, double> > matches;
        for (int i = 0; i < count; ++i)
        {
            const pbio::Recognizer::MatchResult match = recognizer->verifyMatch(*the_template, *templates[i]);
            //matches.push_back(std::make_pair(match.distance, names[i]));
            matches.push_back(std::make_tuple(match.distance, names[i], match.fa_r, match.fr_r));
        }
        // sort matches in order closest first
        std::sort(matches.begin(), matches.end());
        // print results
        //std::cout << "\nResult of identifying face from '" << image_file << "' file:\n";
        for (size_t i = 0; i < matches.size(); ++i)
        {
            const double distance = std::get<0>(matches[i]);
            const std::string name = std::get<1>(matches[i]);
            const double _far = std::get<2>(matches[i]);
            const double _frr = std::get<3>(matches[i]);

            std::map<std::string, std::string> t;
            t.insert(std::pair<std::string, std::string>("name", name));
            t.insert(std::pair<std::string, std::string>("distance", std::to_string(distance)));
            t.insert(std::pair<std::string, std::string>("FAR", std::to_string(_far)));
            t.insert(std::pair<std::string, std::string>("FRR", std::to_string(_frr)));
            res.push_back(t);
        }
        return res;
    });
}

